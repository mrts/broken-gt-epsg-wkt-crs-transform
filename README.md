# Problems with coordinate transformation when using the WKT EPSG authority

This repository contains an example project that demonstrates that converting
coordinates from WGS84 to KKJ succeeds when using the `gt-epsg-hsql` plugin,
but fails when using `gt-epsg-wkt`.

The only difference between the success and failure cases below is the plugin
used:

    diff --git a/pom.xml b/pom.xml
    index ee23627..842478f 100644
    --- a/pom.xml
    +++ b/pom.xml
    @@ -34,7 +34,7 @@
                    </dependency>
                    <dependency>
                            <groupId>org.geotools</groupId>
    -                       <artifactId>gt-epsg-hsql</artifactId>
    +                       <artifactId>gt-epsg-wkt</artifactId>
                            <version>${geotools.version}</version>
                    </dependency>
                    <dependency>

Tests were run with JDK version `1.6.0_45`, but should work equally well with
any other JDK.

## Success case

    git checkout master
    mvn clean test

    ...

    Running com.mrts.crs.CRSTransformTest
    Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.974 sec

## Failure case

    git checkout using-epsg-wkt-breaks-transformation
    mvn clean test

    ...

    Running com.mrts.crs.CRSTransformTest
    Tests run: 2, Failures: 1, Errors: 1, Skipped: 0, Time elapsed: 0.533 sec <<< FAILURE!

    shouldConvertFromWGS84ToKKJ(com.mrts.crs.CRSTransformTest)  Time elapsed: 0.478 sec  <<< ERROR!
    org.geotools.referencing.operation.projection.ProjectionException: The transform result may be 1,182.367 meters away fro
    m the expected position. Are you sure that the input coordinates are inside this map projection area of validity? The po
    int is located 33░12.2'E away from the central meridian and 24░53.0'N away from the latitude of origin. The projection i
    s "Transverse_Mercator".

    shouldConvertFromKKJToWGS84(com.mrts.crs.CRSTransformTest)  Time elapsed: 0.005 sec  <<< FAILURE!
    org.junit.ComparisonFailure: Latitude expected:<[60,2]> but was:<[24,9]>
