package com.mrts.crs;

import org.junit.Test;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;

// Both tests succeed when using gt-epsg-hsql, but fail when using gt-epsg-wkt
public class CRSTransformTest {
	static final double wgs84Latitude = 60.215880370024635;
	static final double wgs84Longitude = 24.88231658935547;
	static final double kkjLatitude = 3382786.8;
	static final double kkjLongitude = 6680140.1;
	static final DecimalFormat df =  new DecimalFormat("#.#");

	@Test
	public void shouldConvertFromWGS84ToKKJ() throws FactoryException, TransformException {
		CRSTransform WGS84ToKKJ = new CRSTransform(CRSParams.wgs84CRS, CRSParams.kkjCRS);
		LatitudeLongitude kkjPoint = WGS84ToKKJ.convert(wgs84Latitude, wgs84Longitude);

		assertEquals("Latitude", df.format(kkjLatitude), df.format(kkjPoint.latitude));
		assertEquals("Longitude", df.format(kkjLongitude), df.format(kkjPoint.longitude));
	}

	@Test
	public void shouldConvertFromKKJToWGS84() throws FactoryException, TransformException {
		CRSTransform KKJToWGS84 = new CRSTransform(CRSParams.kkjCRS, CRSParams.wgs84CRS);
		LatitudeLongitude wgs84Point = KKJToWGS84.convert(kkjLatitude, kkjLongitude);

		assertEquals("Latitude", df.format(wgs84Latitude), df.format(wgs84Point.latitude));
		assertEquals("Longitude", df.format(wgs84Longitude), df.format(wgs84Point.longitude));
	}
}
