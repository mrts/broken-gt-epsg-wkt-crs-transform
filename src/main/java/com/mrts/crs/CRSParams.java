package com.mrts.crs;

public class CRSParams {
	public static final CRSParams kkjCRS = new CRSParams("EPSG:2393", true);
	public static final CRSParams wgs84CRS = new CRSParams("EPSG:4326", false);

	public CRSParams(String CRSName, boolean longitudeFirst) {
		this.CRSName = CRSName;
		this.longitudeFirst = longitudeFirst;
	}

	public final String CRSName;
	public final boolean longitudeFirst;
}
