package com.mrts.crs;

public class LatitudeLongitude {
	public final double latitude;
	public final double longitude;

	public LatitudeLongitude(final double latitude, final double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
}
