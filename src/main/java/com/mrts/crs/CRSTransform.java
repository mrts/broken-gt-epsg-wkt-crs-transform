package com.mrts.crs;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

public class CRSTransform {

	private final MathTransform transformation;
	private final CoordinateReferenceSystem sourceCRS;
	private final CoordinateReferenceSystem targetCRS;

	CRSTransform(final CRSParams sourceCRSParams, final CRSParams targetCRSParams) throws FactoryException {
		this.sourceCRS = CRS.decode(sourceCRSParams.CRSName, sourceCRSParams.longitudeFirst);
		this.targetCRS = CRS.decode(targetCRSParams.CRSName, targetCRSParams.longitudeFirst);
		boolean lenient = true;
		this.transformation = CRS.findMathTransform(sourceCRS, targetCRS, lenient);
	}

	public LatitudeLongitude convert(double latitude, double longitude) throws TransformException {
		DirectPosition2D sourcePosition = new DirectPosition2D(sourceCRS, latitude, longitude);
		DirectPosition2D transformedPosition = (DirectPosition2D) transformation.transform(sourcePosition, new DirectPosition2D(sourceCRS));
		return new LatitudeLongitude(transformedPosition.getX(), transformedPosition.getY());
	}
}
